//
//  MainFlowController.swift
//  ReportManager
//
//  Created by Rameez khan  on 31/10/17.
//  Copyright © 2017 Rameez khan . All rights reserved.
//

import UIKit

class MainFlowController: FlowController {
  let configure: FlowConfigure
  var childFlow: FlowController?
  
  required init(configure: FlowConfigure) {
    self.configure = configure
  }
  
  func start() {
    let navigationController = UINavigationController()
    if let frame = self.configure.window?.bounds {
      navigationController.view.frame = frame
    }
    
    self.configure.window?.rootViewController = navigationController
    self.configure.window?.makeKeyAndVisible()
    let configure = FlowConfigure(window: nil, navigationController: navigationController, parent: self)
    childFlow = PageMenuFlowController(configure: configure)
    childFlow?.start()
    
  }
}
