//
//  PageMenuFlowController.swift
//  ReportManager
//
//  Created by Rameez khan  on 31/10/17.
//  Copyright © 2017 Rameez khan . All rights reserved.
//

import UIKit

class PageMenuFlowController: FlowController {
  let configure: FlowConfigure
  var childFlow: FlowController?
  
  required init(configure: FlowConfigure) {
    self.configure = configure
  }
  
  func start() {
    let viewController = InitialViewController()
    configure.navigationController?.pushViewController(viewController, animated: true)
    
  }
}
