//
//  FlowController.swift
//  ReportManager
//
//  Created by Rameez khan  on 31/10/17.
//  Copyright © 2017 Rameez khan . All rights reserved.
//

import UIKit

enum FlowType {
  case main
  case navigation
}

struct FlowConfigure {
  let window: UIWindow?
  let navigationController: UINavigationController?
  let parent: FlowController?
  
  func whichFlowAmI() -> FlowType? {
    if window != nil { return .main }
    if navigationController != nil { return .navigation }
    return nil
  }
}

protocol FlowController {
  init(configure : FlowConfigure)
  func start()
}
