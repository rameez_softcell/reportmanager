//
//  InitialViewController.swift
//  ReportManager
//
//  Created by Rameez khan  on 31/10/17.
//  Copyright © 2017 Rameez khan . All rights reserved.
//

import UIKit
import PageMenu

class InitialViewController: BaseViewController {

  var pageMenu: CAPSPageMenu?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    var controllerArray : [UIViewController] = []
    var controller : GraphViewController = GraphViewController()
    let navController = UINavigationController(rootViewController: controller)
    controller.title = "SAMPLE TITLE"
    controllerArray.append(controller)
    controllerArray.append(controller)
    controllerArray.append(controller)
    controllerArray.append(controller)
    
    
    // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
    // Example:
    var parameters: [CAPSPageMenuOption] = [.menuItemSeparatorWidth(4.3),
                                            .useMenuLikeSegmentedControl(false),
                                            .menuItemSeparatorPercentageHeight(0.2),
                                            .selectionIndicatorColor(UIColor.blue),
                                            .centerMenuItems(true)
    ]
    
    // Initialize page menu with controller array, frame, and optional parameters
    pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 64, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
    
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    self.view.addSubview(pageMenu!.view)
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
