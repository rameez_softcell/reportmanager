//
//  BaseViewController.swift
//  ReportManager
//
//  Created by Rameez khan  on 31/10/17.
//  Copyright © 2017 Rameez khan . All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
