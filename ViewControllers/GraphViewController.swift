//
//  GraphViewController.swift
//  ReportManager
//
//  Created by Rameez khan  on 01/11/17.
//  Copyright © 2017 Rameez khan . All rights reserved.
//

import UIKit

class GraphViewController: BaseViewController {
  
  private weak var graphView: GraphView? {
    if isViewLoaded {
      return view as? GraphView
    }
    return nil
  }
  override func loadView() {
    super.loadView()
    if let graphView = GraphView.loadFromNib(nibName: "GraphView") {
      view = graphView
    }
    
  }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
